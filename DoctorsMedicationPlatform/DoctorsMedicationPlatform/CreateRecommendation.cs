﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorsMedicationPlatform
{
    public partial class CreateRecommendation : Form
    {

        public WebSoapRequestHandler webRequestHandler { get; set; }

        public CreateRecommendation()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int patientId = Convert.ToInt32(numericUpDown1.Value);
            String message = richTextBox1.Text;
            Boolean result = webRequestHandler.createRecommendation(patientId, message);
            if (result)
            {
                this.label3.Text = "";
                this.Close();
            }
            else
            {
                this.label3.Text += @"Something went wrong!";
            }
        }
    }
}
