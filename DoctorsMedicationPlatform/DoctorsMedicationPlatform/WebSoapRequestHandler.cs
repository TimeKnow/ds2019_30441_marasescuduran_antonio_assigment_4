﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DoctorsMedicationPlatform
{
    public class WebSoapRequestHandler
    {
        public Credentials credentials { get; set; }

        public WebSoapRequestHandler(Credentials credentials)
        {
            this.credentials = credentials;
        }

        public Boolean sendPingRequest()
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(
                @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:gs=""http://www.utcn.ro/weed/salomie"">
                                     <soapenv:Header/>
                                      <soapenv:Body>
                                        <gs:getPingLoginRequest>
                                        </gs:getPingLoginRequest>
                                       </soapenv:Body>
                                       </soapenv:Envelope>");

            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public List<ActivityHistory> getActivityHistory(int patientId)
        {
            List<ActivityHistory> activityHistories = new List<ActivityHistory>();
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(
                $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:gs=""http://www.utcn.ro/weed/salomie"">
                                     <soapenv:Header/>
                                      <soapenv:Body>
                                        <gs:getActivityHistoryRequest>
                                            <gs:patientId>{patientId}</gs:patientId>
                                        </gs:getActivityHistoryRequest>
                                       </soapenv:Body>
                                       </soapenv:Envelope>");
            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }


                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader readStream =
                            new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {

                            XmlDocument soapEnvelopeXmlResponse = new XmlDocument();
                            soapEnvelopeXmlResponse.Load(readStream);
                            XmlNodeList list = soapEnvelopeXmlResponse.GetElementsByTagName("ns2:activityHistoryList");
                            foreach (XmlNode node in list)
                            {
                              
                                XmlNode id = node.ChildNodes[0];
                                XmlNode activity = node.ChildNodes[1];
                                XmlNode brokenRules = node.ChildNodes[2];
                                XmlNode startDate = node.ChildNodes[3];
                                XmlNode endDate = node.ChildNodes[4];
                                activityHistories.Add(new ActivityHistory(Int32.Parse(id.InnerText), activity.InnerText, brokenRules.InnerText,
                                    Int64.Parse(startDate.InnerText), Int64.Parse(endDate.InnerText)));
                            }
                        }

                    }

                    return activityHistories;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return activityHistories;
            }

        }

        public List<MedicationNotification> getMedicationNotifications(int patientId)
        {
            List<MedicationNotification> medicationNotifications = new List<MedicationNotification>();
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(
                $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:gs=""http://www.utcn.ro/weed/salomie"">
                                     <soapenv:Header/>
                                      <soapenv:Body>
                                        <gs:getMedicationNotificationRequest>
                                            <gs:patientId>{patientId}</gs:patientId>
                                        </gs:getMedicationNotificationRequest>
                                       </soapenv:Body>
                                       </soapenv:Envelope>");
            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }


                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader readStream =
                            new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {

                            XmlDocument soapEnvelopeXmlResponse = new XmlDocument();
                            soapEnvelopeXmlResponse.Load(readStream);
                            XmlNodeList list = soapEnvelopeXmlResponse.GetElementsByTagName("ns2:notificationList");
                            foreach (XmlNode node in list)
                            {

                                XmlNode id = node.ChildNodes[0];
                                XmlNode notficationDate = node.ChildNodes[1];
                                XmlNode taken = node.ChildNodes[2];
                                XmlNode medicationId = node.ChildNodes[3];
                                medicationNotifications.Add(new MedicationNotification(
                                    Int32.Parse(id.InnerText),
                                    Int64.Parse(notficationDate.InnerText),
                                    Boolean.Parse(taken.InnerText),
                                    Int32.Parse(medicationId.InnerText)));
                            }
                        }

                    }

                    return medicationNotifications;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return medicationNotifications;
            }

        }

        public Boolean createRecommendation(int patientId, String message)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(
                $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:gs=""http://www.utcn.ro/weed/salomie"">
                                     <soapenv:Header/>
                                      <soapenv:Body>
                                        <gs:addRecommendationRequest>
                                            <gs:recommendation>
                                                <gs:patientId>{patientId}</gs:patientId>
                                                <gs:message>{message}</gs:message>
                                            </gs:recommendation>
                                        </gs:addRecommendationRequest>
                                       </soapenv:Body>
                                       </soapenv:Envelope>");

            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }


                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader readStream =
                            new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {

                            XmlDocument soapEnvelopeXmlResponse = new XmlDocument();
                            soapEnvelopeXmlResponse.Load(readStream);
                            return true;
                        }

                    }

                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<Recommendation> getRecommendations(int patientId)
        {
            List<Recommendation> medicationNotifications = new List<Recommendation>();
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(
                $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:gs=""http://www.utcn.ro/weed/salomie"">
                                     <soapenv:Header/>
                                          <soapenv:Body>
                                                <gs:getRecommendationListRequest>
                                                    <gs:patientId>{patientId}</gs:patientId>
                                                </gs:getRecommendationListRequest>
                                           </soapenv:Body>
                                       </soapenv:Envelope>");

            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader readStream =
                            new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {

                            XmlDocument soapEnvelopeXmlResponse = new XmlDocument();
                            soapEnvelopeXmlResponse.Load(readStream);
                            XmlNodeList list = soapEnvelopeXmlResponse.GetElementsByTagName("ns2:recommendation");
                            foreach (XmlNode node in list)
                            {

                                XmlNode id = node.ChildNodes[0];
                                XmlNode message = node.ChildNodes[2];
                                medicationNotifications.Add(new Recommendation(
                                    Int32.Parse(id.InnerText),
                                    message.InnerText));
                            }
                        }

                    }

                    return medicationNotifications;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return medicationNotifications;
            }

        }

        private HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://localhost:8083/ws");
            webRequest.Headers.Add("Authorization", this.credentials.ToString());
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
    }
}
