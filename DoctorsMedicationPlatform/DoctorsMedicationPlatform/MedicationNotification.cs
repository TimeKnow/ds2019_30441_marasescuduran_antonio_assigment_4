﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorsMedicationPlatform
{
    public class MedicationNotification
    {

        public int id { get; set; }
        public DateTime date { get; set; }
        public Boolean taken { get; set; }
        public int medicationId { get; set; }

        public MedicationNotification(int id, double date, bool taken, int medicationId)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            this.id = id;
            this.date = origin.AddMilliseconds(date);
            this.taken = taken;
            this.medicationId = medicationId;
        }

    }
}
