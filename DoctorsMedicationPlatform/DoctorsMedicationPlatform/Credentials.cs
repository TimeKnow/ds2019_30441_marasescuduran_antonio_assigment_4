﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorsMedicationPlatform
{
    public class Credentials
    {
        public String email { get; set; }
        public String password { get; set; }

        public Credentials(String email, String password)
        {
            this.email = email;
            this.password = password;
        }

        public override string ToString()
        {
            var plainTextCredentials = System.Text.Encoding.UTF8.GetBytes(this.email + ":" + this.password);
            return "Basic " + System.Convert.ToBase64String(plainTextCredentials); ;
        }
    }
}
