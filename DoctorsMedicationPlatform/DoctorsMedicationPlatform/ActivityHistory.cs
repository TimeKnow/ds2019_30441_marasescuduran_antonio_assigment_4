﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorsMedicationPlatform
{
    public class ActivityHistory
    {
        public Int32 id { get; set; }
        public String activity { get; set; }
        public String brokenRules { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }

        public ActivityHistory(int id, string activity, String brokenRules, long start, long end)
        {
            this.id = id;
            this.activity = activity;
            this.brokenRules = brokenRules;
            this.start = new DateTime(start);
            this.end = new DateTime(end);
        }
    }
}
