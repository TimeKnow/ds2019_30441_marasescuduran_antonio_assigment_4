﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorsMedicationPlatform
{
    public class Recommendation
    {

        public Int32 id { get; set; }
        public String message { get; set; }

        public Recommendation(int id, string message)
        {
            this.id = id;
            this.message = message;
        }
        
    }
}
