﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DoctorsMedicationPlatform
{
    public partial class DoctorForm : Form
    {
        public WebSoapRequestHandler webRequestHandler { get; set; }
        private int patientId;
        public DoctorForm()
        {
            InitializeComponent();
            this.button2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.patientId = Convert.ToInt32(numericUpDown1.Value);
            List<ActivityHistory> histories = webRequestHandler.getActivityHistory(this.patientId);
            List<MedicationNotification> notifications = webRequestHandler.getMedicationNotifications(this.patientId);
            List<Recommendation> recommendations = webRequestHandler.getRecommendations(this.patientId);
            Series activityHistorySeries = new Series("Activity History");
            activityHistorySeries.ChartType = SeriesChartType.Bar;
            listBox1.Items.Clear();
            var groups = histories.GroupBy(x => x.activity);
            foreach (var g in groups)
            {
                double value = 0;
                foreach (ActivityHistory h in g.ToList())
                {
                    value += (h.end - h.start).TotalMilliseconds;
                    if (h.brokenRules != "")
                    {
                        listBox1.Items.Add(h.id + " " + h.activity + " " + h.brokenRules);
                    }
                }

                activityHistorySeries.Points.AddXY(g.Key, value);
            }

            listBox2.Items.Clear();
            listBox3.Items.Clear();
            if (notifications.Count == 0)
            {
                this.button2.Enabled = false;
            }
            else
            {
                this.button2.Enabled = true;
            }
            foreach (var n in notifications)
            {
                if (!n.taken)
                {
                    listBox2.Items.Add(n.id + " " + n.date + " MID:" + n.medicationId);
                }
            }

            foreach (var r in recommendations)
            {
                listBox3.Items.Add(r.id + " " + r.message);
            }

            chart1.Series.Clear();
            chart1.Series.Add(activityHistorySeries);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CreateRecommendation form = new CreateRecommendation();
            form.webRequestHandler = this.webRequestHandler;
            form.ShowDialog();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.Text)
            {
                case "Pie":
                    this.chart1.Series[0].ChartType = SeriesChartType.Pie;
                    break;
                case "Plot":
                    this.chart1.Series[0].ChartType = SeriesChartType.Spline;
                    break;
                default:
                    this.chart1.Series[0].ChartType = SeriesChartType.Bar;
                    break;
            }
        }
    }
}
