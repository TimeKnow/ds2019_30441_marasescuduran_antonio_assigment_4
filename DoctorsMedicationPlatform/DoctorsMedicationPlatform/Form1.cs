﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorsMedicationPlatform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoctorForm form = new DoctorForm();
            WebSoapRequestHandler requestHandler = new WebSoapRequestHandler(new Credentials(textBox1.Text, textBox2.Text));
            Boolean response = requestHandler.sendPingRequest();
            if (response)
            {
                label3.Text = "";
                form.webRequestHandler = requestHandler;
                form.ShowDialog();
            }
            else
            {
                label3.Text = @"Invalid email or password!";
            }
        }
    }
}
