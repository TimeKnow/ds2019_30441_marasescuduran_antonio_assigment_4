package com.example.doctorplatform.service;

import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.ast.Not;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.doctorplatform.dto.NotificationCaregiverDTO;
import com.example.doctorplatform.dto.NotificationDTO;
import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.entities.Notification;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.RepositoryFactory;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificationManagementService {
    private final RepositoryFactory repositoryFactory;

    @Transactional
    public void createNotification(NotificationDTO notificationDTO) {
        MediUser patient = repositoryFactory.createUserRepository().findById(notificationDTO.getPatientId()).orElseThrow(ObjectNotFoundException::new);
        Notification notification = new Notification(null, patient, notificationDTO.getStart(),
                notificationDTO.getEnd(), notificationDTO.getActivity(), notificationDTO.getBrokenRules());
        repositoryFactory.createNotificationRepository().save(notification);
        if (notificationDTO.getBrokenRules() != null) {
            String message = "Patient " + notificationDTO.getPatientId() + " has broken the following rules: " + notificationDTO.getBrokenRules();
            System.out.println(message);
            NotificationCaregiverDTO notificationCaregiverDTO = new NotificationCaregiverDTO(notificationDTO.getPatientId(), message);
        }
    }

    @Transactional
    public List<Notification> getAllNotificationsForPatient(Integer patientId) {
        return repositoryFactory.createNotificationRepository().findAll().stream()
                .filter(x->x.getPatient().getMediUserId().equals(patientId)).collect(Collectors.toList());
    }
}
