package com.example.doctorplatform.service;

import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserManagementService {
    private final RepositoryFactory repositoryFactory;
    private final PasswordEncoder encoder;

    public Optional<MediUser> login(String email, String password){
        MediUser user = repositoryFactory.createUserRepository().findByEmail(email).orElseThrow(ObjectNotFoundException::new);
        if(encoder.matches(password, user.getPassword()))
            return Optional.of(user);
        return Optional.empty();
    }
}
