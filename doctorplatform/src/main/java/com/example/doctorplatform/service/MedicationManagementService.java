package com.example.doctorplatform.service;

import com.example.doctorplatform.dto.EnhancedMedicationPlanDTO;
import com.example.doctorplatform.dto.FindByIdsInputDTO;
import com.example.doctorplatform.dto.MedicationDTO;
import com.example.doctorplatform.dto.MedicationPlanDTO;
import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.entities.MediUserRoles;
import com.example.doctorplatform.entities.Medication;
import com.example.doctorplatform.entities.MedicationPlan;
import com.example.doctorplatform.exception.InvalidRequestContent;
import com.example.doctorplatform.exception.ObjectAlreadyExistsException;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.MediUserRepository;
import com.example.doctorplatform.repository.MedicationRepository;
import com.example.doctorplatform.repository.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicationManagementService {
    private final RepositoryFactory repositoryFactory;

    @Transactional
    public void deleteMedication(int id) throws ObjectNotFoundException {
        MedicationRepository medicationRepository = repositoryFactory.createMedicationRepository();
        Medication medication = medicationRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
        medicationRepository.delete(medication);
    }

    @Transactional
    public MedicationDTO updateMedication(int id, MedicationDTO medicationDTO) throws ObjectNotFoundException {
        MedicationRepository medicationRepository = repositoryFactory.createMedicationRepository();
        Medication medication = medicationRepository.findById(id).orElseThrow(ObjectAlreadyExistsException::new);
        medication = medicationDTO.updateMedicationFields(medication);
        return MedicationDTO.ofEntity(medicationRepository.save(medication));
    }

    @Transactional
    public MedicationDTO createMedication(MedicationDTO medicationDTO) throws ObjectAlreadyExistsException {
        return MedicationDTO.ofEntity(repositoryFactory.createMedicationRepository().save(
                MedicationDTO.reverseOfEntity(medicationDTO)));
    }

    @Transactional
    public List<MedicationDTO> getAllMedications() {
        return repositoryFactory.createMedicationRepository().findAll().stream()
                .map(MedicationDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<MedicationDTO> getAllMedicationByIds(FindByIdsInputDTO findByIdsInputDTO) {
        return repositoryFactory.createMedicationRepository().findAll().stream()
                .filter(x -> findByIdsInputDTO.getIds().contains(x.getMedicationId()))
                .map(MedicationDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public MedicationDTO getMedicationById(Integer id) {
        Medication medication = repositoryFactory.createMedicationRepository().findById(id).orElseThrow(ObjectNotFoundException::new);
        return MedicationDTO.ofEntity(medication);
    }

    @Transactional
    public List<MedicationPlanDTO> getAllMedicationPlans() {
        return repositoryFactory.createMedicationPlanRepository().findAll().stream()
                .map(MedicationPlanDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<MedicationPlanDTO> getAllMedicationPlansOfPatient(Integer patientId) {
        return repositoryFactory.createMedicationPlanRepository().findAll().stream()
                .filter(x -> x.getPatient().getMediUserId().equals(patientId))
                .map(MedicationPlanDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<EnhancedMedicationPlanDTO> getAllEnhancedMedicationPlansOfPatient(Integer patientId) {
        return repositoryFactory.createMedicationPlanRepository().findAll().stream()
                .filter(x -> x.getPatient().getMediUserId().equals(patientId))
                .map(EnhancedMedicationPlanDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public MedicationPlanDTO createMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        MediUserRepository mediUserRepository = repositoryFactory.createUserRepository();
        MediUser doctor = mediUserRepository.findById(medicationPlanDTO.getDoctorId()).orElseThrow(ObjectNotFoundException::new);
        if (!doctor.getRole().equals(MediUserRoles.DOCTOR_ROLE))
            throw new InvalidRequestContent();
        MediUser patient = mediUserRepository.findById(medicationPlanDTO.getPatientId()).orElseThrow(ObjectNotFoundException::new);
        if (!patient.getRole().equals(MediUserRoles.PATIENT_ROLE))
            throw new InvalidRequestContent();

        MedicationRepository medicationRepository = repositoryFactory.createMedicationRepository();
        List<Medication> medications = medicationPlanDTO.getMedicationListIds().stream()
                .map(x -> medicationRepository.findById(x).orElseThrow(ObjectNotFoundException::new))
                .collect(Collectors.toList());

        MedicationPlan medicationPlan = new MedicationPlan(doctor, patient, medicationPlanDTO.getIntakeIntervals(), medicationPlanDTO.getPeriod(), medications);
        return MedicationPlanDTO.ofEntity(repositoryFactory.createMedicationPlanRepository().save(medicationPlan));
    }
}
