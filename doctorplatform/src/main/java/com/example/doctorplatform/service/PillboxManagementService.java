package com.example.doctorplatform.service;

import com.example.doctorplatform.dto.PillboxDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.doctorplatform.dto.MedicationPlanStatusDTO;
import com.example.doctorplatform.entities.Medication;
import com.example.doctorplatform.entities.MedicationPlan;
import com.example.doctorplatform.entities.MedicationPlanPillbox;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.RepositoryFactory;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PillboxManagementService {
    private final RepositoryFactory repositoryFactory;

    @Transactional
    public String notifyMedicationTaken(MedicationPlanStatusDTO medicationPlanStatusDTO) {
        MedicationPlan medicationPlan = repositoryFactory.createMedicationPlanRepository()
                .findById(medicationPlanStatusDTO.getMedicationPlanId()).orElseThrow(ObjectNotFoundException::new);
        Medication medication = repositoryFactory.createMedicationRepository()
                .findById(medicationPlanStatusDTO.getMedicationId()).orElseThrow(ObjectNotFoundException::new);
        MedicationPlanPillbox medicationPlanPillbox = new MedicationPlanPillbox(null, medicationPlan, medication,
                true, medicationPlanStatusDTO.getNotificationDate());
        repositoryFactory.createMedicationPlanPillBoxRepository().save(medicationPlanPillbox);
        return "OK";
    }

    @Transactional
    public String notifyMedicationNotTaken(MedicationPlanStatusDTO medicationPlanStatusDTO) {
        MedicationPlan medicationPlan = repositoryFactory.createMedicationPlanRepository()
                .findById(medicationPlanStatusDTO.getMedicationPlanId()).orElseThrow(ObjectNotFoundException::new);
        Medication medication = repositoryFactory.createMedicationRepository()
                .findById(medicationPlanStatusDTO.getMedicationId()).orElseThrow(ObjectNotFoundException::new);
        MedicationPlanPillbox medicationPlanPillbox = new MedicationPlanPillbox(null, medicationPlan, medication,
                false, medicationPlanStatusDTO.getNotificationDate());
        System.out.println("Medication not taken by Patient");
        repositoryFactory.createMedicationPlanPillBoxRepository().save(medicationPlanPillbox);
        return "OK";
    }

    @Transactional
    public List<PillboxDTO> getMedicationPlanPillboxForPatient(Integer patientId){
        return repositoryFactory.createMedicationPlanPillBoxRepository().findAll().stream()
                .filter(x->x.getPlan().getPatient().getMediUserId().equals(patientId))
                .map(PillboxDTO::ofEntity).collect(Collectors.toList());
    }
}
