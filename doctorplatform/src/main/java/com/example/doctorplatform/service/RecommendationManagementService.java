package com.example.doctorplatform.service;

import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.entities.Recommendation;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.utcn.weed.salomie.RecommendationDTO;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RecommendationManagementService {
    private final RepositoryFactory repositoryFactory;

    public Recommendation createRecommendation(RecommendationDTO recommendationDTO) {
        MediUser patient = repositoryFactory.createUserRepository().findById(recommendationDTO.getPatientId())
                .orElseThrow(ObjectNotFoundException::new);
        Recommendation recommendation = new Recommendation(null, patient, recommendationDTO.getMessage());
        return repositoryFactory.createRecommendationRepository().save(recommendation);
    }

    public List<Recommendation> getAllRecommendationsForPatient(Integer patientId) {
        return repositoryFactory.createRecommendationRepository().findAll().stream()
                .filter(x -> x.getPatient().getMediUserId().equals(patientId))
                .collect(Collectors.toList());
    }
}
