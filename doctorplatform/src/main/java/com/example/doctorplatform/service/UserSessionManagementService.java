package com.example.doctorplatform.service;

import com.example.doctorplatform.dto.UserAccountDTO;
import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.exception.ObjectNotFoundException;
import com.example.doctorplatform.repository.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserSessionManagementService implements UserDetailsService {
    private final RepositoryFactory repositoryFactory;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MediUser user = repositoryFactory.createUserRepository().
                findByEmail(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(user.getEmail(), user.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(user.getRole())));
    }

    @Transactional
    public UserAccountDTO loadCurrentUser() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return UserAccountDTO.ofEntity(repositoryFactory.createUserRepository().findByEmail(email)
                .orElseThrow(ObjectNotFoundException::new));
    }
}
