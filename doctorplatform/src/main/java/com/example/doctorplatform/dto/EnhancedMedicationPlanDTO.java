package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.MedicationPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnhancedMedicationPlanDTO {
    private Integer id;
    private Integer doctorId;
    private Integer patientId;
    private String intakeIntervals;
    private String period;
    private List<MedicationDTO> medicationList;

    public static EnhancedMedicationPlanDTO ofEntity(MedicationPlan medicationPlan) {
        return new EnhancedMedicationPlanDTO(medicationPlan.getMedicationPlanId(), medicationPlan.getDoctor().getMediUserId(),
                medicationPlan.getPatient().getMediUserId(), medicationPlan.getIntakeIntervals(), medicationPlan.getPeriod(),
                medicationPlan.getMedicationSet().stream().map(MedicationDTO::ofEntity).collect(Collectors.toList()));
    }
}
