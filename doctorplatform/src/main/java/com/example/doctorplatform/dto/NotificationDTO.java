package com.example.doctorplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDTO {
    private Integer patientId;
    private String activity;
    private Long start;
    private Long end;
    private String brokenRules;
}
