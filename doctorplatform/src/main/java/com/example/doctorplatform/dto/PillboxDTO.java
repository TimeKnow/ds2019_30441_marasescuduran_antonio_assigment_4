package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.MedicationPlanPillbox;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PillboxDTO {
    private Integer id;
    private String notificationDate;
    private Boolean taken;
    private Integer medicationId;

    public static PillboxDTO ofEntity(MedicationPlanPillbox pillbox) {
        return new PillboxDTO(pillbox.getMedicationPlanPillboxId(), pillbox.getNotificationDate(),
                pillbox.getTaken(), pillbox.getMedication().getMedicationId());
    }
}
