package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {
    private Integer id;
    private String name;
    private String sideEffects;
    private String dosage;

    public static MedicationDTO ofEntity(Medication medication) {
        return new MedicationDTO(medication.getMedicationId(), medication.getName(),
                medication.getSideEffects(), medication.getDosage());
    }

    public static Medication reverseOfEntity(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getId(), medicationDTO.getName(),
                medicationDTO.getSideEffects(), medicationDTO.getDosage());
    }

    public Medication updateMedicationFields(Medication medication) {
        medication.setDosage(this.getDosage());
        medication.setName(this.getName());
        medication.setSideEffects(this.getSideEffects());
        return medication;
    }
}
