package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.Medication;
import com.example.doctorplatform.entities.MedicationPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlanDTO {
    private Integer id;
    private Integer doctorId;
    private Integer patientId;
    private String intakeIntervals;
    private String period;
    private List<Integer> medicationListIds;

    public static MedicationPlanDTO ofEntity(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getMedicationPlanId(), medicationPlan.getDoctor().getMediUserId(),
                medicationPlan.getPatient().getMediUserId(), medicationPlan.getIntakeIntervals(), medicationPlan.getPeriod(),
                medicationPlan.getMedicationSet().stream().map(Medication::getMedicationId).collect(Collectors.toList()));
    }
}
