package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.MediUser;
import com.example.doctorplatform.entities.MediUserRoles;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("ALL")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
    private Integer id;
    private String email;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;

    public static PatientDTO ofEntity(MediUser user) {
        return new PatientDTO(user.getMediUserId(), user.getEmail(), user.getName(),
                user.getBirthDate(), user.getGender(), user.getAddress(), user.getMedicalRecord());
    }

    public static MediUser reverseOfEntity(PatientDTO patient) {
        return new MediUser(patient.getId(), patient.getEmail(), "", false, MediUserRoles.PATIENT_ROLE,
                patient.getName(), patient.getBirthDate(), patient.getGender(), patient.getAddress(), patient.getMedicalRecord());
    }

    public MediUser updatePatientFields(MediUser patient) {
        patient.setName(this.getName());
        patient.setEmail(this.getEmail());
        patient.setAddress(this.getAddress());
        patient.setBirthDate(this.getBirthDate());
        patient.setGender(this.getGender());
        patient.setMedicalRecord(this.getMedicalRecord());
        return patient;
    }
}
