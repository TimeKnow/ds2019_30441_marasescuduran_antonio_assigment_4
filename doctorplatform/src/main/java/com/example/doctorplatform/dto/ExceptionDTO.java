package com.example.doctorplatform.dto;

import lombok.Data;

@Data
public class ExceptionDTO {
    private final String message;
}
