package com.example.doctorplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlanStatusDTO {
    private Integer medicationPlanId;
    private Integer medicationId;
    private String notificationDate;
}
