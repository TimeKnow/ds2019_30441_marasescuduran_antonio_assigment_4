package com.example.doctorplatform.dto;

import com.example.doctorplatform.entities.MediUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccountDTO {
    private Integer id;
    private String email;
    private String name;
    private String role;

    public static UserAccountDTO ofEntity(MediUser user) {
        return new UserAccountDTO(user.getMediUserId(), user.getEmail(), user.getName(), user.getRole());
    }
}
