package com.example.doctorplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorplatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctorplatformApplication.class, args);
	}

}
