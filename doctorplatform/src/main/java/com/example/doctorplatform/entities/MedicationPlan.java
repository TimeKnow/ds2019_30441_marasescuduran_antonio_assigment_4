package com.example.doctorplatform.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"medicationSet", "doctor", "patient", "medicationPlanPillbox"})
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medication_plan")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicationPlanId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "doctorId", nullable = false)
    private MediUser doctor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "patientId", nullable = false)
    private MediUser patient;

    private String intakeIntervals;
    private String period;
    @ManyToMany()
    @JoinTable(
            name = "medication_plan_to_medications",
            joinColumns = {@JoinColumn(name = "medicationPlanId")},
            inverseJoinColumns = {@JoinColumn(name = "medicationId")}
    )
    private Set<Medication> medicationSet = new HashSet<>();

    @OneToMany(mappedBy = "plan", fetch = FetchType.EAGER)
    private Set<MedicationPlanPillbox> medicationPlanPillbox;

    public MedicationPlan(MediUser doctor, MediUser patient, String intakeIntervals, String period, List<Medication> medications) {
        this.doctor = doctor;
        this.patient = patient;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
        this.medicationSet = new HashSet<>(medications);
        this.medicationSet.forEach(m -> m.getMedicationPlanSet().add(this));
    }
}
