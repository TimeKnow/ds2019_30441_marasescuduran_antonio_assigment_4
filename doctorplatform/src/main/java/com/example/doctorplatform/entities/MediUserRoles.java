package com.example.doctorplatform.entities;

public class MediUserRoles {
    public static final String DOCTOR_ROLE = "DOCTOR";
    public static final String PATIENT_ROLE = "PATIENT";
    public static final String CAREGIVER_ROLE = "CAREGIVER";
}
