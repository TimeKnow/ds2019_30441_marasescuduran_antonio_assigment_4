package com.example.doctorplatform.exception;

public class EmailAlreadyExistsException extends RuntimeException {
    public EmailAlreadyExistsException() {
        super("The email is already in use!");
    }
}
