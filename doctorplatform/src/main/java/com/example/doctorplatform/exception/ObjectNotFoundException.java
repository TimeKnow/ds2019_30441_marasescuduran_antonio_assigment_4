package com.example.doctorplatform.exception;

public class ObjectNotFoundException extends RuntimeException {
    public ObjectNotFoundException() {
        super("The required object was not found!");
    }
}
