package com.example.doctorplatform.repository;

import com.example.doctorplatform.entities.MedicationPlan;
import org.springframework.data.repository.Repository;

public interface MedicationPlanRepository extends Repository<MedicationPlan, Integer>, CRUDRepository<MedicationPlan> {
}
