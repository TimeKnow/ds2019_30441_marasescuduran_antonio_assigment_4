package com.example.doctorplatform.repository;

import com.example.doctorplatform.entities.Medication;
import org.springframework.data.repository.Repository;

public interface MedicationRepository extends Repository<Medication, Integer>, CRUDRepository<Medication> {
}
