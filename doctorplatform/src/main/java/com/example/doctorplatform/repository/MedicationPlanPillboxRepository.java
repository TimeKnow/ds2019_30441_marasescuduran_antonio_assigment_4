package com.example.doctorplatform.repository;

import com.example.doctorplatform.entities.MedicationPlanPillbox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanPillboxRepository extends JpaRepository<MedicationPlanPillbox, Integer> {
}
