package com.example.doctorplatform.repository;

import com.example.doctorplatform.entities.MediUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MediUserRepository extends JpaRepository<MediUser, Integer> {
    Optional<MediUser> findByEmail(String email);
}
