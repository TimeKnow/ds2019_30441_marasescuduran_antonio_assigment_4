package com.example.doctorplatform.repository;

import com.example.doctorplatform.entities.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {
}
