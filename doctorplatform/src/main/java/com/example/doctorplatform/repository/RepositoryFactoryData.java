package com.example.doctorplatform.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RepositoryFactoryData implements RepositoryFactory {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final MediUserRepository userRepository;
    private final NotificationRepository notificationRepository;
    private final MedicationPlanPillboxRepository medicationPlanPillboxRepository;
    private final RecommendationRepository recommendationRepository;


    @Override
    public MedicationPlanRepository createMedicationPlanRepository() {
        return medicationPlanRepository;
    }

    @Override
    public MedicationRepository createMedicationRepository() {
        return medicationRepository;
    }

    @Override
    public MediUserRepository createUserRepository() {
        return userRepository;
    }

    @Override
    public NotificationRepository createNotificationRepository() {
        return notificationRepository;
    }

    @Override
    public MedicationPlanPillboxRepository createMedicationPlanPillBoxRepository() {
        return this.medicationPlanPillboxRepository;
    }

    @Override
    public RecommendationRepository createRecommendationRepository() {
        return this.recommendationRepository;
    }
}
