package com.example.doctorplatform.endpoint;

import com.example.doctorplatform.dto.PillboxDTO;
import com.example.doctorplatform.entities.Notification;
import com.example.doctorplatform.service.NotificationManagementService;
import com.example.doctorplatform.service.PillboxManagementService;
import com.example.doctorplatform.service.RecommendationManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.utcn.weed.salomie.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Endpoint
@RequiredArgsConstructor
public class DoctorEndpoint {
    private static final String NAMESPACE_URI = "http://www.utcn.ro/weed/salomie";

    private final NotificationManagementService notificationManagementService;
    private final PillboxManagementService pillboxManagementService;
    private final RecommendationManagementService recommendationManagementService;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPingLoginRequest")
    @ResponsePayload
    public GetPingLoginResponse getActivityHistory(@RequestPayload GetPingLoginRequest request) {
        GetPingLoginResponse response = new GetPingLoginResponse();
        response.setMessage("OK");
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityHistoryRequest")
    @ResponsePayload
    public GetActivityHistoryResponse getActivityHistory(@RequestPayload GetActivityHistoryRequest request) {
        GetActivityHistoryResponse response = new GetActivityHistoryResponse();
        List<Notification> notifications = notificationManagementService.getAllNotificationsForPatient(request.getPatientId());
        notifications.stream().map(DoctorEndpoint::mapNotificationToActivityHistory)
                .forEach(x -> response.getActivityHistoryList().add(x));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationNotificationRequest")
    @ResponsePayload
    public GetMedicationNotificationResponse getMedicationNotifications(@RequestPayload GetMedicationNotificationRequest request) {
        GetMedicationNotificationResponse response = new GetMedicationNotificationResponse();
        List<PillboxDTO> pillboxDTOS = pillboxManagementService.getMedicationPlanPillboxForPatient(request.getPatientId());
        pillboxDTOS.stream()
                .map(DoctorEndpoint::mapPillboxObjectToMedicationNotification)
                .forEach(x -> response.getNotificationList().add(x));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addRecommendationRequest")
    @ResponsePayload
    public AddRecommendationResponse addRecommendationResponse(@RequestPayload AddRecommendationRequest recommendationRequest) {
        com.example.doctorplatform.entities.Recommendation recommendation =
                recommendationManagementService.createRecommendation(recommendationRequest.getRecommendation());
        AddRecommendationResponse response = new AddRecommendationResponse();
        response.setRecommendation(transformToRecommendation(recommendation));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRecommendationListRequest")
    @ResponsePayload
    public GetRecommendationListResponse getRecommendationListRequest(@RequestPayload GetRecommendationListRequest recommendationListRequest) {
        List<com.example.doctorplatform.entities.Recommendation> recommendations =
                recommendationManagementService.getAllRecommendationsForPatient(recommendationListRequest.getPatientId());

        List<Recommendation> responseBody = recommendations.stream().map(DoctorEndpoint::transformToRecommendation).collect(Collectors.toList());
        GetRecommendationListResponse response = new GetRecommendationListResponse();
        response.getRecommendation().addAll(responseBody);
        return response;
    }


    public static ActivityHistory mapNotificationToActivityHistory(Notification notification) {
        ActivityHistory activityHistory = new ActivityHistory();
        activityHistory.setId(notification.getId());
        activityHistory.setActivity(notification.getActivity());
        String brokenRules = notification.getBrokenRules();
        activityHistory.setBrokenRule((brokenRules != null) ? brokenRules : "");
        activityHistory.setStartDate(notification.getStartDate().toString());
        activityHistory.setEndDate(notification.getEndDate().toString());
        return activityHistory;
    }

    public static MedicationNotification mapPillboxObjectToMedicationNotification(PillboxDTO pillboxDTO) {
        MedicationNotification notification = new MedicationNotification();
        notification.setId(pillboxDTO.getId());
        notification.setNotificationDate(pillboxDTO.getNotificationDate());
        notification.setMedicationId(pillboxDTO.getMedicationId());
        notification.setTaken(pillboxDTO.getTaken());
        return notification;
    }

    public static Recommendation transformToRecommendation(com.example.doctorplatform.entities.Recommendation recommendation) {
        Recommendation responseBody = new Recommendation();
        responseBody.setId(recommendation.getId());
        responseBody.setPatientId(recommendation.getPatient().getMediUserId());
        responseBody.setMessage(recommendation.getMessage());
        return responseBody;
    }
}
