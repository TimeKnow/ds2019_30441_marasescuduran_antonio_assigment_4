class MedicationPlanStatusModel:
    def __init__(self, medication_plan_id, medication_id, notification_date):
        self.medication_plan_id = medication_plan_id
        self.medication_id = medication_id
        self.notification_date = notification_date
