import functools
from datetime import datetime

import PySimpleGUI as sg

from core.models.medication_plan_status import MedicationPlanStatusModel
from core.utils.utils import flatmap_medications_from_plans, formatt_medication_plan_to_str, \
    get_next_medications_to_be_taken, find_medication_plan_for_medication_id, get_not_taken_medications, \
    remove_medication_from_plans
from pillbox_pb2 import MedicationPlanStatus

sg.ChangeLookAndFeel('Black')
sg.SetOptions(element_padding=(10, 10))


class PillboxApp:
    def __init__(self, service, predefined_time):
        self.predefined_time = predefined_time
        self.medication_plans = []
        self.medications = []
        self.next_medications = []
        self.not_taken_medications = []
        self.frozen_not_taken_medications = []
        self.medication_plan_formatted = []
        self.service = service
        self.layout = [[sg.Text('Time: ', size=(8, 2), font=('Helvetica', 16), justification='center'),
                        sg.Text('', size=(8, 2), font=('Helvetica', 20), justification='left', key='current_time')],
                       [sg.Text('Next Medication IDs: ', size=(25, 2), font=('Helvetica', 16), justification='left'),
                        sg.Text('', size=(20, 2), font=('Helvetica', 20), justification='left',
                                key='next_medications')],
                       [sg.Text('Not Taken Medication IDs: ', size=(25, 2), font=('Helvetica', 16),
                                justification='left'),
                        sg.Text('', size=(20, 2), font=('Helvetica', 20), justification='left',
                                key='not_taken_medications')],
                       [sg.Text('Medications', font=('Helvetica', 12))],
                       [sg.Listbox(values=self.medication_plans, size=(100, 20), key='medication_list',
                                   enable_events=True)],
                       [sg.Button('Take Medication', key='take_medication_event', button_color=('black', 'white'),
                                  size=(50, 1)),
                        sg.Exit(button_color=('black', 'white'), key='Exit', size=(35, 1))]]
        self.window = sg.Window('Window Title', self.layout, no_titlebar=True, auto_size_buttons=False,
                                keep_on_top=True, grab_anywhere=True)

    def run(self):
        paused = False
        while True:
            if not paused:
                event, values = self.window.read(timeout=10)
            else:
                event, values = self.window.read()
            if event is None or event == 'Exit':
                break
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            current_date_time = now.strftime("Y-%m-%d %H:%M:%S")
            if current_time == self.predefined_time:
                self.medication_plans = self.service.get_medication_plans_for_patients()
                self.update_list()

            if event == 'take_medication_event':
                selected_index = self.window['medication_list'].GetIndexes()[0]
                selected_value = self.medication_plan_formatted[selected_index]
                selected_medication_id = int(selected_value.split(' ')[0])
                selected_medication = [x for x in self.medications if x['id'] == selected_medication_id][0]
                selected_medication_plan = find_medication_plan_for_medication_id(self.medication_plans,
                                                                                  selected_medication_id)
                status = MedicationPlanStatusModel(selected_medication_plan['id'], selected_medication['id'],
                                                   current_date_time)
                response = self.service.notify_medication_taken(status)
                if response == "OK":
                    self.medication_plans = remove_medication_from_plans(self.medication_plans, selected_medication_id)
                    self.update_list()
            self.on_time_change(current_time, current_date_time)
        self.service.close()

    def on_time_change(self, current_time, current_date_time):
        self.not_taken_medications = get_not_taken_medications(self.medication_plans)
        self.next_medications = get_next_medications_to_be_taken(self.medication_plans)
        if len(self.not_taken_medications) > 0:
            self.notify_on_not_taken_medications(current_date_time)
        self.window['current_time'].update(current_time)
        self.update_elements()

    def update_elements(self):
        formatted_next_medications = functools.reduce((lambda accum, y: str(accum) + ' ' + str(y['id'])),
                                                      self.next_medications, '')
        formatted_not_taken_medications = functools.reduce((lambda accum, y: str(accum) + ' ' + str(y['id'])),
                                                           self.frozen_not_taken_medications, '')
        self.window['not_taken_medications'].update(formatted_not_taken_medications)
        self.window['next_medications'].update(formatted_next_medications)

    def update_list(self):
        self.medications = flatmap_medications_from_plans(self.medication_plans)
        self.medication_plan_formatted = formatt_medication_plan_to_str(self.medication_plans)
        self.window['medication_list'].update(self.medication_plan_formatted)

    def notify_on_not_taken_medications(self, current_date_time):
        self.frozen_not_taken_medications = []
        for medication in self.not_taken_medications:
            selected_medication_plan = find_medication_plan_for_medication_id(self.medication_plans,
                                                                              medication['id'])
            status = MedicationPlanStatusModel(selected_medication_plan['id'], medication['id'],
                                               current_date_time)
            response = self.service.notify_medication_not_taken(status)
            if response == "OK":
                self.medication_plans = remove_medication_from_plans(self.medication_plans, medication['id'])
                self.frozen_not_taken_medications.append(medication)
                self.update_list()
