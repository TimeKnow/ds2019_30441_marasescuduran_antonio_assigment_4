package utcn.MediPlatform.proto;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import org.lognet.springboot.grpc.GRpcService;
import utcn.MediPlatform.dto.EnhancedMedicationPlanDTO;
import utcn.MediPlatform.dto.MedicationDTO;
import utcn.MediPlatform.dto.MedicationPlanStatusDTO;
import utcn.MediPlatform.proto.service.*;
import utcn.MediPlatform.service.MedicationManagementService;
import utcn.MediPlatform.service.PillboxManagementService;

import java.util.List;
import java.util.stream.Collectors;

@GRpcService
@AllArgsConstructor
public class PillboxServiceImpl extends PillboxServiceGrpc.PillboxServiceImplBase {
    private final MedicationManagementService medicationManagementService;
    private final PillboxManagementService pillboxManagementService;

    @Override
    public void getMedicationPlan(PatientRPC request, StreamObserver<ListMedicationPlanRPC> responseObserver) {
        Integer patientId = request.getId();
        List<EnhancedMedicationPlanDTO> medicationPlans = medicationManagementService.getAllEnhancedMedicationPlansOfPatient(patientId);
        Iterable<MedicationPlanRPC> medicationPlanRPCS = this.mapMedicationPlanToRPC(medicationPlans);
        ListMedicationPlanRPC medicationPlanRPC = ListMedicationPlanRPC.newBuilder().addAllMedicationPlans(medicationPlanRPCS).build();
        responseObserver.onNext(medicationPlanRPC);
        responseObserver.onCompleted();
    }

    @Override
    public void notifyMedicationTaken(MedicationPlanStatus request, StreamObserver<BasicResponse> responseObserver) {
        MedicationPlanStatusDTO medicationPlanStatusDTO = new MedicationPlanStatusDTO(request.getMedicationPlanId(),
                request.getMedicationId(), request.getNotificationDate());
        String response = pillboxManagementService.notifyMedicationTaken(medicationPlanStatusDTO);
        responseObserver.onNext(BasicResponse.newBuilder().setMessage(response).build());
        responseObserver.onCompleted();
    }

    @Override
    public void notifyMedicationNotTaken(MedicationPlanStatus request, StreamObserver<BasicResponse> responseObserver) {
        MedicationPlanStatusDTO medicationPlanStatusDTO = new MedicationPlanStatusDTO(request.getMedicationPlanId(),
                request.getMedicationId(), request.getNotificationDate());
        String response = pillboxManagementService.notifyMedicationNotTaken(medicationPlanStatusDTO);
        responseObserver.onNext(BasicResponse.newBuilder().setMessage(response).build());
        responseObserver.onCompleted();
    }

    private Iterable<MedicationPlanRPC> mapMedicationPlanToRPC(List<EnhancedMedicationPlanDTO> medicationPlans) {
        return medicationPlans.stream().map(x -> MedicationPlanRPC.newBuilder()
                .setId(x.getId())
                .setDoctorId(x.getDoctorId())
                .setPatientId(x.getPatientId())
                .setIntakeIntervals(x.getIntakeIntervals())
                .setPeriod(x.getPeriod())
                .addAllMedications(mapMedicationsToRPC(x.getMedicationList()))
                .build()
        ).collect(Collectors.toList());
    }

    private Iterable<MedicationRPC> mapMedicationsToRPC(List<MedicationDTO> medications) {
        return medications.stream().map(x -> MedicationRPC.newBuilder()
                .setId(x.getId())
                .setName(x.getName())
                .setDosage(x.getDosage())
                .setSideEffects(x.getSideEffects())
                .build()
        ).collect(Collectors.toList());
    }
}
