package utcn.MediPlatform.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import utcn.MediPlatform.dto.CaregiverDTO;
import utcn.MediPlatform.dto.DoctorDTO;
import utcn.MediPlatform.dto.FindByIdsInputDTO;
import utcn.MediPlatform.dto.PatientDTO;
import utcn.MediPlatform.service.HospitalPersoneelManagementService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class HospitalPersoneelController {
    private final HospitalPersoneelManagementService hospitalPersoneelManagementService;

    @GetMapping("/api/patients")
    public List<PatientDTO> readAllPatients() {
        return hospitalPersoneelManagementService.getAllPatients();
    }

    @GetMapping("/api/caregivers")
    public List<CaregiverDTO> readAllCaregivers() {
        return hospitalPersoneelManagementService.getAllCaregivers();
    }

    @GetMapping("api/doctors")
    public List<DoctorDTO> readAllDoctors() {
        return hospitalPersoneelManagementService.getAllDoctors();
    }

    @PostMapping("api/patients/find-by-ids")
    public List<PatientDTO> readPatientsByIds(@RequestBody FindByIdsInputDTO findByIdsInputDTO) {
        return hospitalPersoneelManagementService.getAllPatientByIds(findByIdsInputDTO);
    }

    @GetMapping("/api/caregivers/{id}")
    public CaregiverDTO readCaregiver(@PathVariable("id") Integer id) {
        return hospitalPersoneelManagementService.getCaregiverById(id);
    }

    @PostMapping("api/caregivers/find-by-ids")
    public List<CaregiverDTO> readCaregiversByIds(@RequestBody FindByIdsInputDTO findByIdsInputDTO) {
        return hospitalPersoneelManagementService.getAllCaregiversByIds(findByIdsInputDTO);
    }

    @GetMapping("/api/patients/{id}")
    public PatientDTO readPatient(@PathVariable("id") Integer id) {
        return hospitalPersoneelManagementService.getPatientById(id);
    }

    @GetMapping("/api/caregivers/{id}/patients")
    public List<PatientDTO> readAllPatientsOfCaregiver(@PathVariable("id") Integer id) {
        return hospitalPersoneelManagementService.getPatientsForCaregiver(id);
    }

    @PostMapping("/api/caregivers/{caregiverId}/patients/{patientId}")
    public PatientDTO attachPatientToCaregiver(@PathVariable("caregiverId") Integer caregiverId,
                                                     @PathVariable("patientId") Integer patientId) {
        return hospitalPersoneelManagementService.attachPatientToCaregiver(caregiverId, patientId);
    }

    @PostMapping("/api/patients")
    public PatientDTO createPatient(@RequestBody PatientDTO patientDTO) {
        return hospitalPersoneelManagementService.createPatient(patientDTO);
    }

    @PostMapping("/api/caregivers")
    public CaregiverDTO createCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return hospitalPersoneelManagementService.createCaregiver(caregiverDTO);
    }

    @PutMapping("/api/patients/{id}")
    public PatientDTO updatePatient(@PathVariable("id") Integer id, @RequestBody PatientDTO patientDTO) {
        return hospitalPersoneelManagementService.updatePatient(id, patientDTO);
    }

    @PutMapping("/api/caregivers/{id}")
    public CaregiverDTO updateCaregiver(@PathVariable("id") Integer id, @RequestBody CaregiverDTO caregiverDTO) {
        return hospitalPersoneelManagementService.updateCaregiver(id, caregiverDTO);
    }

    @DeleteMapping("/api/patients/{id}")
    public void deletePatient(@PathVariable("id") Integer id) {
        hospitalPersoneelManagementService.deleteUser(id);
    }

    @DeleteMapping("/api/caregivers/{id}")
    public void deleteCaregiver(@PathVariable("id") Integer id) {
        hospitalPersoneelManagementService.deleteUser(id);
    }

}
