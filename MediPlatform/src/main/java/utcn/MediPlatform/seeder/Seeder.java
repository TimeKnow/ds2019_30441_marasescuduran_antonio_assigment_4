package utcn.MediPlatform.seeder;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utcn.MediPlatform.entities.MediUser;
import utcn.MediPlatform.entities.Medication;
import utcn.MediPlatform.entities.MedicationPlan;
import utcn.MediPlatform.entities.MedicationPlanPillbox;
import utcn.MediPlatform.repository.*;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static utcn.MediPlatform.entities.MediUserRoles.CAREGIVER_ROLE;
import static utcn.MediPlatform.entities.MediUserRoles.DOCTOR_ROLE;
import static utcn.MediPlatform.entities.MediUserRoles.PATIENT_ROLE;

@Component
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class Seeder implements CommandLineRunner {
    private final RepositoryFactory factory;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void run(String... args) {
        MedicationRepository medicationRepository = factory.createMedicationRepository();
        MediUserRepository mediUserRepository = factory.createUserRepository();
        MedicationPlanRepository medicationPlanRepository = factory.createMedicationPlanRepository();
        MedicationPlanPillboxRepository medicationPlanPillboxRepository = factory.createMedicationPlanPillBoxRepository();

        List<MediUser> mediUsers = new ArrayList<>();
        List<Medication> medications = new ArrayList<>();
        List<MedicationPlan> medicationPlans = new ArrayList<>();

        if (mediUserRepository.findAll().isEmpty()) {
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "doctor@email.com", passwordEncoder.encode("doctor"),
                            true, DOCTOR_ROLE, "Doctor Hugh Laurie", "09/20/2019",
                            "MALE", "Helm Street")
            ));

            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "patient01@email.com", passwordEncoder.encode("patient01"),
                            true, PATIENT_ROLE, "Patient 01", "09/21/2019",
                            "MALE", "Male Street", "Ciuma, Bubonic Plague")
            ));
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "patient02@email.com", passwordEncoder.encode("patient02"),
                            true, PATIENT_ROLE, "Patient 02", "09/22/2019",
                            "MALE", "Male Street", "Ciuma, Bubonic Plague")
            ));
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "patient03@email.com", passwordEncoder.encode("patient03"),
                            true, PATIENT_ROLE, "Patient 03", "09/22/2019",
                            "MALE", "Male Street", "Ciuma, Bubonic Plague")
            ));
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "caregiver01@email.com", passwordEncoder.encode("caregiver01"),
                            true, CAREGIVER_ROLE, "Caregiver 01", "09/22/2019",
                            "MALE", "Male Street", mediUsers.get(1), mediUsers.get(2), mediUsers.get(3))
            ));
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "caregiver02@email.com", passwordEncoder.encode("caregiver02"),
                            true, CAREGIVER_ROLE, "Caregiver 02", "09/22/2019",
                            "MALE", "Male Street", mediUsers.get(2))
            ));
            mediUsers.add(mediUserRepository.save(
                    new MediUser(null, "caregiver03@email.com", passwordEncoder.encode("caregiver03"),
                            true, CAREGIVER_ROLE, "Caregiver 03", "09/22/2019",
                            "MALE", "Male Street", mediUsers.get(3))
            ));
        }

        if (medicationRepository.findAll().isEmpty()) {
            medications.add(medicationRepository.save(new Medication(null, "Medication 01",
                    "Death", "1ml")));
            medications.add(medicationRepository.save(new Medication(null, "Medication 02",
                    "Possible Death", "2ml")));
            medications.add(medicationRepository.save(new Medication(null, "Medication 03",
                    "Eventual Death", "3ml")));
        }
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date start = new Date(System.currentTimeMillis());
        Date end = new Date(System.currentTimeMillis() + 25000);
        if (medicationPlanRepository.findAll().isEmpty()) {
            medicationPlans.add(medicationPlanRepository.save(new MedicationPlan(mediUsers.get(0), mediUsers.get(1), formatter.format(start) + " " + formatter.format(end), "19.11.2019-25.11.2019", medications)));
            medicationPlans.add(medicationPlanRepository.save(new MedicationPlan(mediUsers.get(0), mediUsers.get(2), formatter.format(start) + " " + formatter.format(end), "19.11.2019-25.11.2019", medications.subList(0, 1))));
        }

        if (medicationPlanPillboxRepository.findAll().isEmpty()) {
            SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
            Date date = new Date(System.currentTimeMillis());
            medicationPlanPillboxRepository.save(new MedicationPlanPillbox(null, medicationPlans.get(0), medications.get(0), false, String.valueOf(date.getTime())));
            medicationPlanPillboxRepository.save(new MedicationPlanPillbox(null, medicationPlans.get(0), medications.get(2), true, String.valueOf(date.getTime())));
            medicationPlanPillboxRepository.save(new MedicationPlanPillbox(null, medicationPlans.get(1), medications.get(0), false, String.valueOf(date.getTime())));
            medicationPlanPillboxRepository.save(new MedicationPlanPillbox(null, medicationPlans.get(0), medications.get(1), false, String.valueOf(date.getTime())));
        }
    }

    @Transactional
    public void clear() {
        CRUDRepository<Medication> medicationCRUDRepository = factory.createMedicationRepository();
        MediUserRepository mediUserCRUDRepository = factory.createUserRepository();

        medicationCRUDRepository.findAll().forEach(medicationCRUDRepository::delete);
        mediUserCRUDRepository.findAll().forEach(mediUserCRUDRepository::delete);
    }
}
