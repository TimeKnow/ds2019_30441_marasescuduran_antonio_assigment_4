package utcn.MediPlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MediPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediPlatformApplication.class, args);
	}

}
