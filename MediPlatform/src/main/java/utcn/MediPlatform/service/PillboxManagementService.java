package utcn.MediPlatform.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import utcn.MediPlatform.dto.MedicationDTO;
import utcn.MediPlatform.dto.MedicationPlanStatusDTO;
import utcn.MediPlatform.entities.Medication;
import utcn.MediPlatform.entities.MedicationPlan;
import utcn.MediPlatform.entities.MedicationPlanPillbox;
import utcn.MediPlatform.exception.ObjectNotFoundException;
import utcn.MediPlatform.repository.RepositoryFactory;

@Service
@RequiredArgsConstructor
public class PillboxManagementService {
    private final RepositoryFactory repositoryFactory;

    @Transactional
    public String notifyMedicationTaken(MedicationPlanStatusDTO medicationPlanStatusDTO) {
        MedicationPlan medicationPlan = repositoryFactory.createMedicationPlanRepository()
                .findById(medicationPlanStatusDTO.getMedicationPlanId()).orElseThrow(ObjectNotFoundException::new);
        Medication medication = repositoryFactory.createMedicationRepository()
                .findById(medicationPlanStatusDTO.getMedicationId()).orElseThrow(ObjectNotFoundException::new);
        MedicationPlanPillbox medicationPlanPillbox = new MedicationPlanPillbox(null, medicationPlan, medication,
                true, medicationPlanStatusDTO.getNotificationDate());
        repositoryFactory.createMedicationPlanPillBoxRepository().save(medicationPlanPillbox);
        return "OK";
    }

    @Transactional
    public String notifyMedicationNotTaken(MedicationPlanStatusDTO medicationPlanStatusDTO) {
        MedicationPlan medicationPlan = repositoryFactory.createMedicationPlanRepository()
                .findById(medicationPlanStatusDTO.getMedicationPlanId()).orElseThrow(ObjectNotFoundException::new);
        Medication medication = repositoryFactory.createMedicationRepository()
                .findById(medicationPlanStatusDTO.getMedicationId()).orElseThrow(ObjectNotFoundException::new);
        MedicationPlanPillbox medicationPlanPillbox = new MedicationPlanPillbox(null, medicationPlan, medication,
                false, medicationPlanStatusDTO.getNotificationDate());
        System.out.println("Medication not taken by Patient");
        repositoryFactory.createMedicationPlanPillBoxRepository().save(medicationPlanPillbox);
        return "OK";
    }
}
