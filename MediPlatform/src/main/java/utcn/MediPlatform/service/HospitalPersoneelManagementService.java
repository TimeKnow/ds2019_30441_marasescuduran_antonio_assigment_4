package utcn.MediPlatform.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import utcn.MediPlatform.dto.*;
import utcn.MediPlatform.entities.MediUser;
import utcn.MediPlatform.entities.Medication;
import utcn.MediPlatform.exception.EmailAlreadyExistsException;
import utcn.MediPlatform.exception.InvalidRequestContent;
import utcn.MediPlatform.exception.ObjectAlreadyExistsException;
import utcn.MediPlatform.exception.ObjectNotFoundException;
import utcn.MediPlatform.repository.*;

import java.util.List;
import java.util.stream.Collectors;

import static utcn.MediPlatform.entities.MediUserRoles.CAREGIVER_ROLE;
import static utcn.MediPlatform.entities.MediUserRoles.DOCTOR_ROLE;
import static utcn.MediPlatform.entities.MediUserRoles.PATIENT_ROLE;

@Service
@RequiredArgsConstructor
public class HospitalPersoneelManagementService {
    private final RepositoryFactory repositoryFactory;

    @Transactional
    public List<PatientDTO> getAllPatients() {
        return repositoryFactory.createUserRepository().findAll().stream().
                filter(x -> x.getRole().equals(PATIENT_ROLE)).map(PatientDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<PatientDTO> getAllPatientByIds(FindByIdsInputDTO findByIdsInputDTO) {
        return repositoryFactory.createUserRepository().findAll().stream()
                .filter(x -> x.getRole().equals(PATIENT_ROLE) && findByIdsInputDTO.getIds().contains(x.getMediUserId()))
                .map(PatientDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<DoctorDTO> getAllDoctors() {
        return repositoryFactory.createUserRepository().findAll().stream()
                .filter(x -> x.getRole().equals(DOCTOR_ROLE)).map(DoctorDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public PatientDTO getPatientById(Integer id) {
        MediUser patient = repositoryFactory.createUserRepository().findById(id).orElseThrow(ObjectNotFoundException::new);
        if (!patient.getRole().equals(PATIENT_ROLE))
            throw new InvalidRequestContent();
        return PatientDTO.ofEntity(patient);
    }

    @Transactional
    public CaregiverDTO getCaregiverById(Integer id) {
        MediUser caregiver = repositoryFactory.createUserRepository().findById(id).orElseThrow(ObjectNotFoundException::new);
        if (!caregiver.getRole().equals(CAREGIVER_ROLE))
            throw new InvalidRequestContent();
        return CaregiverDTO.ofEntity(caregiver);
    }

    @Transactional
    public List<PatientDTO> getPatientsForCaregiver(Integer id) {
        MediUserRepository mediUserRepository = repositoryFactory.createUserRepository();
        MediUser caregiver = mediUserRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
        if (!caregiver.getRole().equals(CAREGIVER_ROLE))
            throw new InvalidRequestContent();
        return caregiver.getPatients().stream().map(PatientDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public PatientDTO attachPatientToCaregiver(Integer caregiverId, Integer patientId) {
        MediUserRepository mediUserRepository = repositoryFactory.createUserRepository();
        MediUser caregiver = mediUserRepository.findById(caregiverId).orElseThrow(ObjectNotFoundException::new);
        MediUser patient = mediUserRepository.findById(patientId).orElseThrow(ObjectNotFoundException::new);
        if (caregiver.getPatients().contains(patient))
            throw new ObjectAlreadyExistsException();
        caregiver.getPatients().add(patient);
        mediUserRepository.save(caregiver);
        return PatientDTO.ofEntity(patient);
    }

    @Transactional
    public List<CaregiverDTO> getAllCaregivers() {
        return repositoryFactory.createUserRepository().findAll().stream().
                filter(x -> x.getRole().equals(CAREGIVER_ROLE)).map(CaregiverDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<CaregiverDTO> getAllCaregiversByIds(FindByIdsInputDTO findByIdsInputDTO) {
        return repositoryFactory.createUserRepository().findAll().stream()
                .filter(x -> x.getRole().equals(DOCTOR_ROLE) && findByIdsInputDTO.getIds().contains(x.getMediUserId()))
                .map(CaregiverDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public PatientDTO createPatient(PatientDTO patientDTO) throws ObjectAlreadyExistsException {
        MediUserRepository userCRUDRepository = repositoryFactory.createUserRepository();
        if (userCRUDRepository.findByEmail(patientDTO.getEmail()).isPresent())
            throw new EmailAlreadyExistsException();
        return PatientDTO.ofEntity(userCRUDRepository.save(PatientDTO.reverseOfEntity(patientDTO)));
    }

    @Transactional
    public CaregiverDTO createCaregiver(CaregiverDTO caregiverDTO) throws ObjectAlreadyExistsException {
        MediUserRepository userCRUDRepository = repositoryFactory.createUserRepository();
        if (userCRUDRepository.findByEmail(caregiverDTO.getEmail()).isPresent())
            throw new EmailAlreadyExistsException();
        return CaregiverDTO.ofEntity(userCRUDRepository.save(CaregiverDTO.reverseOfEntity(caregiverDTO)));
    }


    @Transactional
    public PatientDTO updatePatient(int id, PatientDTO patientDTO) throws ObjectNotFoundException {
        MediUserRepository userCRUDRepository = repositoryFactory.createUserRepository();
        MediUser currentPatient = userCRUDRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
        currentPatient = patientDTO.updatePatientFields(currentPatient);
        return PatientDTO.ofEntity(userCRUDRepository.save(currentPatient));
    }

    @Transactional
    public CaregiverDTO updateCaregiver(int id, CaregiverDTO caregiverDTO) throws ObjectNotFoundException {
        MediUserRepository userCRUDRepository = repositoryFactory.createUserRepository();
        MediUser currentCaregiver = userCRUDRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
        currentCaregiver = caregiverDTO.updateCaregiverFields(currentCaregiver);
        return CaregiverDTO.ofEntity(userCRUDRepository.save(currentCaregiver));
    }


    @Transactional
    public void deleteUser(int id) throws ObjectNotFoundException {
        MediUserRepository mediUserCRUDRepository = repositoryFactory.createUserRepository();
        MediUser user = mediUserCRUDRepository.findById(id).orElseThrow(ObjectNotFoundException::new);
        user.clear();
        mediUserCRUDRepository.save(user);
        mediUserCRUDRepository.delete(user);
    }

}
