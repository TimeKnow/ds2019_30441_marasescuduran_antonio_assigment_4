package utcn.MediPlatform.listener;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import utcn.MediPlatform.dto.NotificationDTO;
import utcn.MediPlatform.service.NotificationManagementService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

@Component
@RequiredArgsConstructor
public class SensorListener {
    private final NotificationManagementService notificationManagementService;

    public void receiveMessage(HashMap<String, String> message) {
        String activity = message.get("activity");
        Integer patientId = Integer.valueOf(message.get("patient_id"));
        long start = Long.parseLong(message.get("start"));
        long end = Long.parseLong(message.get("end"));
        LocalDateTime startDateTime = Instant.ofEpochMilli(start).atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime endDateTime = Instant.ofEpochMilli(end).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String brokenRules = null;
        switch (activity) {
            case "Sleeping":
                if (getHourDifference(startDateTime, endDateTime) >= 12)
                    brokenRules = "R1";
                break;
            case "Leaving":
                if (getHourDifference(startDateTime, endDateTime) >= 12)
                    brokenRules = "R2";
                break;
            case "Toileting":
            case "Showering":
                if (getHourDifference(startDateTime, endDateTime) >= 1)
                    brokenRules = "R3";
                break;
        }
        NotificationDTO notificationDTO = new NotificationDTO(patientId, activity, start, end, brokenRules);
        notificationManagementService.createNotification(notificationDTO);
    }

    private Long getHourDifference(LocalDateTime start, LocalDateTime end) {
        LocalDateTime tempDateTime = LocalDateTime.from(start);

        long years = tempDateTime.until(end, ChronoUnit.YEARS);
        tempDateTime = tempDateTime.plusYears(years);

        long months = tempDateTime.until(end, ChronoUnit.MONTHS);
        tempDateTime = tempDateTime.plusMonths(months);

        long days = tempDateTime.until(end, ChronoUnit.DAYS);
        tempDateTime = tempDateTime.plusDays(days);

        return tempDateTime.until(end, ChronoUnit.HOURS);
    }
}
