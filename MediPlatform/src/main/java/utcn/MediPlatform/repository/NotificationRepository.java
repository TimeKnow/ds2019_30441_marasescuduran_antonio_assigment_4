package utcn.MediPlatform.repository;

import org.springframework.data.repository.Repository;
import utcn.MediPlatform.entities.Notification;

public interface NotificationRepository extends Repository<Notification, Integer>, CRUDRepository<Notification> {
}
