package utcn.MediPlatform.repository;

public interface RepositoryFactory {
    MedicationPlanRepository createMedicationPlanRepository();

    MedicationRepository createMedicationRepository();

    MediUserRepository createUserRepository();

    NotificationRepository createNotificationRepository();

    MedicationPlanPillboxRepository createMedicationPlanPillBoxRepository();

    RecommendationRepository createRecommendationRepository();
}
