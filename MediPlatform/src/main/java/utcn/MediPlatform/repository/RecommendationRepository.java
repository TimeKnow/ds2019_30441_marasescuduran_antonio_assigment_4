package utcn.MediPlatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import utcn.MediPlatform.entities.Recommendation;

public interface RecommendationRepository extends JpaRepository<Recommendation, Integer> {
}
