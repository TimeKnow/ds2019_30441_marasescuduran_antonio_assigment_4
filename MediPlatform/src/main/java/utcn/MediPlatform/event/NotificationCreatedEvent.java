package utcn.MediPlatform.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import utcn.MediPlatform.dto.NotificationCaregiverDTO;

@Data
@EqualsAndHashCode(callSuper = true)
public class NotificationCreatedEvent extends BaseEvent {
    private final NotificationCaregiverDTO notificationCaregiverDTO;

    public NotificationCreatedEvent(NotificationCaregiverDTO notificationCaregiverDTO) {
        super(EventType.NOTIFICATION_CREATED);
        this.notificationCaregiverDTO = notificationCaregiverDTO;
    }
}
