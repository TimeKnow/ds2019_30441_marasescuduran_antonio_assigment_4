package utcn.MediPlatform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import utcn.MediPlatform.entities.MediUser;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccountDTO {
    private Integer id;
    private String email;
    private String name;
    private String role;

    public static UserAccountDTO ofEntity(MediUser user) {
        return new UserAccountDTO(user.getMediUserId(), user.getEmail(), user.getName(), user.getRole());
    }
}
