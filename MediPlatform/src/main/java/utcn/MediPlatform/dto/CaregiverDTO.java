package utcn.MediPlatform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import utcn.MediPlatform.entities.MediUser;
import utcn.MediPlatform.entities.MediUserRoles;

@SuppressWarnings("ALL")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverDTO {
    private Integer id;
    private String email;
    private String name;
    private String birthDate;
    private String gender;
    private String address;

    public static CaregiverDTO ofEntity(MediUser user) {
        return new CaregiverDTO(user.getMediUserId(), user.getEmail(), user.getName(),
                user.getBirthDate(), user.getGender(), user.getAddress());
    }

    public static MediUser reverseOfEntity(CaregiverDTO caregiverDTO) {
        return new MediUser(caregiverDTO.getId(), caregiverDTO.getEmail(), "", false,
                MediUserRoles.CAREGIVER_ROLE, caregiverDTO.getName(), caregiverDTO.getBirthDate(), caregiverDTO.getGender(),
                caregiverDTO.getAddress());
    }

    public MediUser updateCaregiverFields(MediUser caregiver) {
        caregiver.setName(this.getName());
        caregiver.setEmail(this.getEmail());
        caregiver.setAddress(this.getAddress());
        caregiver.setBirthDate(this.getBirthDate());
        caregiver.setGender(this.getGender());
        return caregiver;
    }
}
