package utcn.MediPlatform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import utcn.MediPlatform.entities.MediUser;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDTO {
    private Integer id;
    private String email;
    private String name;
    private String role;

    public static DoctorDTO ofEntity(MediUser doctor) {
        return new DoctorDTO(doctor.getMediUserId(), doctor.getEmail(), doctor.getName(), doctor.getRole());
    }
}
