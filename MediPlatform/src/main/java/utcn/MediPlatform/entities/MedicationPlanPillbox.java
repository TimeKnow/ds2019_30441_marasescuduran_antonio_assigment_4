package utcn.MediPlatform.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medication_plan_pillbox")
public class MedicationPlanPillbox {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicationPlanPillboxId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "medicationPlanId", nullable = false)
    private MedicationPlan plan;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "medicationId", nullable = false)
    private Medication medication;

    private Boolean taken;
    private String notificationDate;
}
