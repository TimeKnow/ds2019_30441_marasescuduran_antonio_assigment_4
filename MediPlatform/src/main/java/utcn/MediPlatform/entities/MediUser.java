package utcn.MediPlatform.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity(name = "medi_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medi_user")
public class MediUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer mediUserId;
    private String email;
    private String password;
    private Boolean hasAccount;
    private String role;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;

    public MediUser(Integer id) {
        this.mediUserId = id;
    }

    @OneToMany(mappedBy = "doctor")
    private Set<MedicationPlan> doctorPlans;

    @OneToMany(mappedBy = "patient")
    private Set<MedicationPlan> medicationPlans;

    @OneToMany(mappedBy = "patient")
    private Set<Notification> notifications;

    @OneToMany(mappedBy = "patient")
    private Set<Recommendation> recommendations;

    @ManyToMany
    @JoinTable(name = "patient_caregivers",
            joinColumns = @JoinColumn(name = "patientId"),
            inverseJoinColumns = @JoinColumn(name = "caregiverId")
    )
    private List<MediUser> caregivers = new ArrayList<>();

    @ManyToMany(mappedBy = "caregivers")
    private List<MediUser> patients;

    public MediUser(Integer id, String email, String password, Boolean hasAccount, String role, String name,
                    String birthDate, String gender, String address, MediUser... patients) {
        this.mediUserId = id;
        this.email = email;
        this.password = password;
        this.hasAccount = hasAccount;
        this.role = role;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = Stream.of(patients).collect(Collectors.toList());
        this.patients.forEach(p -> p.getCaregivers().add(this));
    }

    public MediUser(Integer id, String email, String password, Boolean hasAccount, String role, String name,
                    String birthDate, String gender, String address, String medicalRecord) {
        this.mediUserId = id;
        this.email = email;
        this.password = password;
        this.hasAccount = hasAccount;
        this.role = role;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public void removePatient(MediUser patient) {
        this.patients.remove(patient);
        patient.getCaregivers().remove(this);
    }

    public void removeCaregiver(MediUser caregiver) {
        this.caregivers.remove(caregiver);
        caregiver.getPatients().remove(this);
    }

    public void clear() {
        this.caregivers.forEach(x -> {
            x.getCaregivers().remove(this);
            x.getPatients().remove(this);
        });
        this.patients.forEach(x -> {
            x.getCaregivers().remove(this);
            x.getPatients().remove(this);
        });
        this.caregivers.clear();
        this.patients.clear();
    }
}
