package utcn.MediPlatform.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"medicationPlanSet", "medicationPlanPillbox"})
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medication")
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicationId;
    private String name;
    private String sideEffects;
    private String dosage;

    @ManyToMany(mappedBy = "medicationSet")
    private Set<MedicationPlan> medicationPlanSet = new HashSet<>();

    @OneToMany(mappedBy = "medication", fetch = FetchType.EAGER)
    private Set<MedicationPlanPillbox> medicationPlanPillbox;


    public Medication(Integer medicationId, String name, String sideEffects, String dosage) {
        this.medicationId = medicationId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }
}
